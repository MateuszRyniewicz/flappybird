﻿
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private HighScoreTable highScoreTable;


    [Header("UI System")]
    [SerializeField] private Text scoreText;
    [SerializeField] private GameObject playButton;
    [SerializeField] private GameObject gameOver;
    [SerializeField] private GameObject highscoreTableUI;

    public int pipesCouter;

    [Header("SpriteBombs")]
    public SpriteRenderer[] displayBombs;

    private readonly int maxBombs = 3;
    [HideInInspector]
    public int currentBomb = 1;


    private bool bombTrue;
    private int currentScore;
    private int highScore;
    private PlayerController player;



    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        player = GameObject.FindObjectOfType<PlayerController>();
        if (player == null)
            Debug.Log("player in Game Manager is null");


        highScoreTable = GameObject.FindObjectOfType<HighScoreTable>();
        if (highScoreTable == null)
            Debug.Log("highScoreTable in Manager is null");
    }

    private void Start()
    {
        PlayGame();
    }

    public void Update()
    {
        CalculateBombSystem();
        UpdateBobbDisplay();
    }


    public void PlayGame()
    {
        HighscoreTableDisableAndDisplayBombOnScreen();

        highScore = PlayerPrefs.GetInt("HighScore");

        playButton.SetActive(false);
        gameOver.SetActive(false);


        currentScore = 0;
        scoreText.text = currentScore.ToString();

        Time.timeScale = 1f;
        player.enabled = true;
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        player.enabled = false;
    }
       

    public void GameOver()
    {
      
        gameOver.SetActive(true);
        playButton.SetActive(true);
        GameManager.instance.currentBomb = 0;


        PauseGame();
        HighscoreTableOpen();
        PipesToDestroy();
    }


    public void AddScore(int scoreValue)
    {
        currentScore += scoreValue;
        scoreText.text = currentScore.ToString();

        if (currentScore > 1)
        {
            highScore = currentScore;
            highScoreTable.AddHighscoreEntry(highScore);
            PlayerPrefs.SetInt("HighScore", highScore);    
        }
    }
    public void CalculateBombSystem()
    {
        if (pipesCouter >= 2)
        {
            bombTrue = true;
            pipesCouter = 0;
        }
        else
        {
            bombTrue = false;
        }

        if (bombTrue)
        {
            currentBomb++;
            if (currentBomb > 3)
            {
                currentBomb = 3;
            }
        }
    }

    public void UpdateBobbDisplay()
    {
        switch (currentBomb)
        {
            case 3:
                displayBombs[0].GetComponent<SpriteRenderer>().enabled = true;
                displayBombs[1].GetComponent<SpriteRenderer>().enabled = true;
                displayBombs[2].GetComponent<SpriteRenderer>().enabled = true;
                break;
            case 2:
                displayBombs[0].GetComponent<SpriteRenderer>().enabled = true;
                displayBombs[1].GetComponent<SpriteRenderer>().enabled = true;
                displayBombs[2].GetComponent<SpriteRenderer>().enabled = false;
                break;
            case 1:
                displayBombs[0].GetComponent<SpriteRenderer>().enabled = true;
                displayBombs[1].GetComponent<SpriteRenderer>().enabled = false;
                displayBombs[2].GetComponent<SpriteRenderer>().enabled = false;
                break;
            case 0:
                displayBombs[0].GetComponent<SpriteRenderer>().enabled = false;
                displayBombs[1].GetComponent<SpriteRenderer>().enabled = false;
                displayBombs[2].GetComponent<SpriteRenderer>().enabled = false;
                break;
        }
    }

    public void HighscoreTableOpen()
    {
        highscoreTableUI.gameObject.SetActive(true);
    }

    public void HighscoreTableDisableAndDisplayBombOnScreen()
    {
        highscoreTableUI.gameObject.SetActive(false);

        for (int i = 0; i < displayBombs.Length; i++)
        {
            displayBombs[i].GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public void PipesToDestroy()
    {
        Pipes[] piepesToDestroy = FindObjectsOfType<Pipes>();

        if (piepesToDestroy.Length == 0)
            return;

        for (int i = 0; i < piepesToDestroy.Length; i++)
        {
            Destroy(piepesToDestroy[i].gameObject);
        }
    }


}

