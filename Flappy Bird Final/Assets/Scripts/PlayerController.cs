﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Value")]
    [SerializeField] private float gravity = -9.8f;
    [SerializeField] private float powerJump = 7f;
    [SerializeField] private int limitBomb;


    private Vector3 dir;
    private SpriteRenderer sr;

    private void Awake()
    {

        sr = GetComponentInChildren<SpriteRenderer>();
       // InputBombAndRemovePiepes();

    }

    private void Start()
    {
       // InputBombAndRemovePiepes();
    }


    void FixedUpdate()
    {

        InputJump();

        if (Input.touchCount <= 0)
        {
            return;
        }

        if (Input.GetTouch(0).tapCount == 2)
        {
            InputBombAndRemovePiepes();
        }
    }


    public void InputJump()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            dir = Vector3.up * powerJump;
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began)
            {
                dir = Vector3.up * powerJump;
            }


        }
        
        if(Input.GetKey(KeyCode.B)) //|| Input.GetTouch(0).tapCount == 2)
        {
            if (GameManager.instance.currentBomb > 0)
            {

                InputBombAndRemovePiepes();
            }
        }

        dir.y += gravity * Time.fixedDeltaTime;
        transform.position += dir * Time.fixedDeltaTime;
    }

    public void InputBombAndRemovePiepes()
    {
            GameManager.instance.currentBomb--; 

            Pipes[] piepesToDestroy = FindObjectsOfType<Pipes>();

            if (piepesToDestroy.Length == 0)
                return;



            for (int i = 0; i < piepesToDestroy.Length; i++)
            {
                Destroy(piepesToDestroy[i].gameObject);
            }
    }


    private void OnEnable()
    {
        Vector3 pos = transform.position;
        pos.y = 0f;
        transform.position = pos;
        dir = Vector3.zero;
    }


    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Obstacle")
        {
            GameManager.instance.GameOver();
        }

        if(other.tag == "Score")
        {
            GameManager.instance.AddScore(1);
            GameManager.instance.pipesCouter++;
            
        }
    }
}
