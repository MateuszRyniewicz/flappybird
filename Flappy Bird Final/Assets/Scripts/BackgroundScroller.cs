﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{

   

   

    [SerializeField] Transform BG1, BG2;
    [SerializeField] float scroolSpeed;

    private float bgWidth;


    void Start()
    {
        bgWidth = BG1.GetComponent<SpriteRenderer>().bounds.size.x;
       
    }
 
    void FixedUpdate()
    {
        BG1.position -= new Vector3(scroolSpeed * Time.fixedDeltaTime, 0f, 0f);
        BG2.position -= new Vector3(scroolSpeed * Time.fixedDeltaTime, 0f ,0f);

        if (BG1.position.x < -bgWidth)
        {
            BG1.position += new Vector3(bgWidth * 2, 0f, 0f);
        }

        if (BG2.position.x < -bgWidth)
        {
            BG2.position += new Vector3(bgWidth * 2f, 0f, 0f);
        }
    }
}
