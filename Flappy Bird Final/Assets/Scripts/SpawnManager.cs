﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private GameObject pipesPrefab;
    [SerializeField] private float spawnRate = 1f;
    [SerializeField] private float minHeight = -10f;
    [SerializeField] private float maxHeight = 10f;

    private void OnEnable()
    {
        InvokeRepeating(nameof(Spawner), spawnRate, spawnRate);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    public void Spawner()
    {
        GameObject newPipes = Instantiate(pipesPrefab, transform.position, Quaternion.identity);
        newPipes.transform.position += Vector3.up * Random.Range(minHeight, maxHeight);
    }
}
